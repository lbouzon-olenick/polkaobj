﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolKaOO
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            webBrowser1.ScriptErrorsSuppressed = true;
        }

         private void GetWebPage_Click(object sender, EventArgs e)
        {
            var TapaClarin = new TapaDiario("https://www.clarin.com/");
            LinkList.DataSource = TapaClarin.ObtenerListaLinks();
        }


        private void PolkaCounterButton_Click(object sender, EventArgs e)
        {
            Noticia NoticiaClarin = new Noticia (LinkList.SelectedItem.ToString());
            Reporte rep = new Reporte(NoticiaClarin);
            richTextBox1.Text = rep.polka + "  " + rep.kapol + "  " + rep.noticia.notiLink;
            listBox1.Items.Add (richTextBox1.Text);
        }

        private void goToLinkButton_Click(object sender, EventArgs e)
        {
            webBrowser1.Url = new System.Uri(Convert.ToString(LinkList.SelectedItem));
        }
    }
}
