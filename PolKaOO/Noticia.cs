﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace PolKaOO
{
    public class Noticia
    {

        #region ATRIBUTOS
        public string notiLink { get; set; }
        private string htmlCode;
        #endregion

        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        public Noticia(string link)
        {
            notiLink = link;
            htmlCode = HtmlGetter(notiLink);
        }


        /// <summary>
        /// Baja el html de la noticia. El link de la noticia es el argumento.
        /// </summary>
        private string HtmlGetter(string newsLink)
        {

            using (WebClient client = new WebClient { Encoding = Encoding.UTF8 })
            {
                return client.DownloadString(newsLink);
            }
        }

        /// <summary>
        /// Encuentra el cuerpo de la nota en el html de la noticia.
        /// </summary>
        public List<string> TextFinder()
        {

            //borrar epigrafe => < h5 class="mfp-epigrafe">Macri y Malcorra.EFE/ PRESIDENCIA</h5>
            //borrar epigrafe => < h5 * </h5>


            string titleDector = "\"articleBody\">";
            int minimo = titleDector.Length;
            int inicio = htmlCode.IndexOf(titleDector) + minimo;
            int fin = htmlCode.IndexOf("/n", inicio + 1);
            int lengh = fin - inicio +1;

            string lineaCuepo = htmlCode.Substring(inicio, lengh);

            string code = ("(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)");
            lineaCuepo = Regex.Replace(lineaCuepo, code, String.Empty, RegexOptions.Singleline);
            lineaCuepo = Regex.Replace(lineaCuepo, @"<(.+?)>", String.Empty, RegexOptions.Singleline);
            lineaCuepo = lineaCuepo.Replace(".", ".\r\n");

            return   lineaCuepo.Split('.').ToList();

        }


    }
}


