﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PolKaOO
{
    public class Reporte
    {

        #region ATRIBUTOS

        /// <summary>
        /// Contador de oraciones con "K" antes que "polémico"
        /// </summary>
        public int kapol { get; set; }

        /// <summary>
        /// Contador de oraciones con "K" depues que "polmémico"
        /// </summary>
        public int polka { get; set; }


        public Noticia noticia { get; set; }

        ///// <summary>
        ///// Link de la noticia de la cual se genera el reporte.
        ///// /// </summary>
        //public string Articlelink;

        ///// <summary>
        ///// lista de oraciones obtenidos de la Noticia.
        ///// </summary>
        //List<string> ListaOraciones;
        #endregion



        /// <summary>
        /// Constructor. Dos variables de ingreso. El el link de la nota y una lista con las oraciones.
        /// </summary>
        /// <param name="Link"></param>
        /// <param name="oraciones"></param>
        public Reporte(Noticia _noticia)
        {
            this.noticia = _noticia;
            polkaCounter();

        }

        // Metodos.
        /// <summary>
        /// Contador de Polka o Kapol.
        /// </summary>
        private void polkaCounter()
        {
            string letraK = "[\\, \\. \\s \"]?K[\\, \\. \\s \"]?";
            string palabraPol = "[P p]olémic[\\, \\. \\s a o \"]?";

           // foreach (string oracion in ListaOraciones)
            foreach (string oracion in noticia.TextFinder())
                {
                if (Regex.IsMatch(oracion, letraK) && Regex.IsMatch(oracion, palabraPol))

                {
                    if (oracion.IndexOf(letraK) < oracion.IndexOf(palabraPol))
                    {
                        kapol ++;
                    }
                    else
                    {
                        polka ++;
                    }
                }
            }

        }
    }
}