﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Windows.Forms;


namespace PolKaOO
{
    public class TapaDiario
    {

        #region ATRIBUTOS
        public string webAdress { get; set;  }



        /// <summary>
        /// Codigo html de de la pagina web.
        /// </summary>
        private string htmlCode;

        private HashSet<string> htmlLinks ;
        #endregion

        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        public TapaDiario(string mainLink)
        {
            webAdress = mainLink;
            htmlCode = HtmlSourceGetter(webAdress);
            LinkFinder();

              }



        #region METODOS
        /// <summary>
        /// Toma el html de el webBroser1 dado como argumento.
        /// </summary>
        /// <param name="linkArgument"></param>
        /// <returns></returns>
        private string HtmlSourceGetter(string linkArgument)
        {
            using (WebClient client = new WebClient { Encoding = Encoding.UTF8 })
            {
                return client.DownloadString(linkArgument);
            }
        }


        /// <summary>
        /// Ecuentra los links de las notas en el codigo.
        /// </summary>
        private void LinkFinder()
        {
         string[] streamCode;
         HashSet<string> links = new HashSet<string>();
         streamCode = htmlCode.Split(new string[] { "\n" }, StringSplitOptions.None);

            for (int i = 0; i < streamCode.Length; i++)
            {
                if (streamCode[i].Contains("<a href=\"/"))
                    {
                    string temp = streamCode[i].ToString().Trim();
                    links.Add(temp.Trim());
                }
            }
            htmlLinks =  links;
        }

        /// <summary>
        /// Elimina el código que no forman parte de el texto.
        /// </summary>
        /// <returns>Me devuelve una Lista<> con las oraciones del cuerpo de la nota.</returns>
        public List<string> ObtenerListaLinks()
        {
            List<string> trimLine = new List<string>();
            string temp;

            foreach (string element in htmlLinks)
            {
                if (element.StartsWith("<a href=\"/") && element.Contains(".html"))

                {
                    temp = element.Replace("<a href=\"/", string.Empty);
                    temp = temp.Remove(temp.IndexOf("\""));
                    if (!temp.StartsWith("http"))
                    {
                        temp = webAdress + temp;
                    }
                    trimLine.Add(temp);

                }
            }

            return trimLine;
        }



        #endregion METODOS
    }
}