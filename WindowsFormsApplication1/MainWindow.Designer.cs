﻿namespace WindowsFormsApplication1
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GetWebPage = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.goToLinkButton = new System.Windows.Forms.Button();
            this.LinkList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // GetWebPage
            // 
            this.GetWebPage.Location = new System.Drawing.Point(13, 13);
            this.GetWebPage.Name = "GetWebPage";
            this.GetWebPage.Size = new System.Drawing.Size(160, 56);
            this.GetWebPage.TabIndex = 0;
            this.GetWebPage.Text = "Get Page Links";
            this.GetWebPage.UseVisualStyleBackColor = true;
            this.GetWebPage.Click += new System.EventHandler(this.GetWebPage_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(13, 310);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(981, 315);
            this.webBrowser1.TabIndex = 15;
            this.webBrowser1.Url = new System.Uri("https://www.clarin.com/", System.UriKind.Absolute);
            // 
            // goToLinkButton
            // 
            this.goToLinkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.goToLinkButton.Location = new System.Drawing.Point(865, 12);
            this.goToLinkButton.Name = "goToLinkButton";
            this.goToLinkButton.Size = new System.Drawing.Size(115, 56);
            this.goToLinkButton.TabIndex = 24;
            this.goToLinkButton.Text = "Go To Link";
            this.goToLinkButton.UseVisualStyleBackColor = true;
            this.goToLinkButton.Click += new System.EventHandler(this.goToLinkButton_Click);
            // 
            // LinkList
            // 
            this.LinkList.FormattingEnabled = true;
            this.LinkList.Location = new System.Drawing.Point(12, 75);
            this.LinkList.Name = "LinkList";
            this.LinkList.Size = new System.Drawing.Size(968, 225);
            this.LinkList.TabIndex = 27;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 637);
            this.Controls.Add(this.LinkList);
            this.Controls.Add(this.goToLinkButton);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.GetWebPage);
            this.Name = "MainWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button GetWebPage;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button goToLinkButton;
        private System.Windows.Forms.ListBox LinkList;
    }
}

