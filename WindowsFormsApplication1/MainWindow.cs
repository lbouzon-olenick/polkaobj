﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WindowsFormsApplication1
{
    public partial class MainWindow : Form
    {

        public MainWindow()
        {
            InitializeComponent();
            webBrowser1.ScriptErrorsSuppressed = true;
        }
        public void GetWebPage_Click(object sender, EventArgs e)
        {
            LinkList.Items.Clear();
            TapaDiario TapaClarin = new TapaDiario("https://www.clarin.com/");
            foreach (Noticia noticia in TapaClarin.ListaDeNoticias)
            {
                LinkList.Items.Add  (noticia.polka + " " + noticia.kapol + " " + noticia.webAdress);
            }

        }

        private void goToLinkButton_Click(object sender, EventArgs e)
        {

            if (!(LinkList.SelectedItem  == null))
            {
                int httpPosition  = LinkList.SelectedItem.ToString().IndexOf("http");
                string selectedLink = LinkList.SelectedItem.ToString().Substring(httpPosition);
                webBrowser1.Url = new System.Uri(selectedLink);
            }
            else
            { MessageBox.Show("Cargue un link"); }
        }

    }
}
