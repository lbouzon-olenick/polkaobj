﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public class Noticia : TapaDiario
    {
        List<string> textNoticia;

        public Noticia(string mainLink) : base(mainLink)
        {
            textNoticia = TextFinder();
            polkaCounter();
        }

        /// <summary>
        /// Contador de oraciones con "K" antes que "polémico"
        /// </summary>
        public int kapol { get; set; }

        /// <summary>
        /// Contador de oraciones con "K" depues que "polmémico"
        /// </summary>
        public int polka { get; set; }


        /// <summary>
        /// Encuentra el cuerpo de la nota en el html de la noticia.
        /// </summary>
        public List<string> TextFinder()
        {
            //borrar epigrafe => < h5 class="mfp-epigrafe">Macri y Malcorra.EFE/ PRESIDENCIA</h5>
            //borrar epigrafe => < h5 * </h5>

            string titleDector = "\"articleBody\">";
            string code = ("(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)");


            int minimo = titleDector.Length;
            int inicio = htmlCode.IndexOf(titleDector) + minimo;
            int fin = htmlCode.IndexOf("\n", inicio + 1);
            int lengh = fin - inicio + 1;

            string lineaCuepo = htmlCode.Substring(inicio, lengh);

            lineaCuepo = Regex.Replace(lineaCuepo, code, String.Empty, RegexOptions.Singleline);
            lineaCuepo = Regex.Replace(lineaCuepo, @"<(.+?)>", String.Empty, RegexOptions.Singleline);
            lineaCuepo = lineaCuepo.Replace(".", ".\r\n");

            return lineaCuepo.Split('.').ToList();

        }

        /// <summary>
        /// Contador de Polka o Kapol.
        /// </summary>
        private void polkaCounter()
        {
            string letraK = "[\\, \\. \\s \"]?K[\\, \\. \\s \"]?";
            string palabraPol = "[P p]olémic[\\, \\. \\s a o \"]?";
            foreach (string sentence in textNoticia)
            {
                if (Regex.IsMatch(sentence, letraK) && Regex.IsMatch(sentence, palabraPol))
                {
                    if (sentence.IndexOf(letraK) < sentence.IndexOf(palabraPol))
                    {
                        kapol++;
                    }
                    else
                    {
                        polka++;
                    }
                }
            }
        }
    }
}



