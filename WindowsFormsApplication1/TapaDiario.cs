﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Windows.Forms;


namespace WindowsFormsApplication1
{
    public class TapaDiario
    {

        #region ATRIBUTOS
        public string webAdress { get; set; }

        // => Tuve que pasarlo a publica sino no se puede obtener desde child.
        public string htmlCode;

        // => Tuve que pasarlo a publica sino no se puede obtener desde child.
        public List<Noticia> ListaDeNoticias = new List<Noticia>();


        private HashSet<string> htmlLinks;
        #endregion

        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        public TapaDiario(string _link)
        {
            webAdress = _link;
            htmlCode = HtmlGetter();


            LinkFinder();
            CreateNoticias();
        }



        #region METODOS
        /// <summary>
        /// Toma el html de el webBroser1 dado como argumento.
        /// </summary>
        /// <returns>Html Source code in a string</returns>
        public string HtmlGetter()
        {
            using (WebClient client = new WebClient { Encoding = Encoding.UTF8 })
            {
                return client.DownloadString(webAdress);
            }
        }

        /// <summary>
        /// Ecuentra los links de las notas en el codigo.
        /// </summary>
        private void LinkFinder()
        {
            string[] streamCode = htmlCode.Split(new string[] { "\n" }, StringSplitOptions.None); ;
            HashSet<string> links = new HashSet<string>();
            string newsLink;

            //MessageBox.Show((this.GetType().ToString()));

            foreach (string linea in streamCode)
            {

                if (
                     (linea.Trim().StartsWith("<a href=\"/")
                     || linea.Trim().StartsWith("<a class=\"content-ico center\" href=\"/")
                     || linea.Trim().StartsWith("<a class=\"content-ico md\" href=\"/")
                     )
                     && linea.Contains(".html"))
                {
                    newsLink = linea.Trim();

                    newsLink = newsLink.Replace("<a href=\"/", string.Empty);
                    newsLink = newsLink.Replace("<a class=\"content-ico center\" href=\"/", string.Empty);
                    newsLink = newsLink.Replace("<a class=\"content-ico md\" href=\"/", string.Empty);

                    newsLink = newsLink.Remove(newsLink.IndexOf("\""));

                    if (!newsLink.StartsWith("http"))
                    {
                        newsLink = webAdress + newsLink;
                    }

                    links.Add(newsLink.ToString().Trim());
                }


            }
            htmlLinks = links;
        }

        /// <summary>
        /// Crea una noticia por cada link encontrado en el HTML.
        /// </summary>
        /// <returns>Crea en LisaDeNoticias<Noticia> una noticia por cada link en el HTML.</returns>
        private void CreateNoticias()
        {
            foreach (string element in htmlLinks)
            {

                ListaDeNoticias.Add(new Noticia(element));

            }
        }
        #endregion METODOS
    }

}
